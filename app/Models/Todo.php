<?php

namespace App\Models;

use Illuminate\Broadcasting\Channel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Tonysm\TurboLaravel\Models\Broadcasts;

class Todo extends Model
{
    use HasFactory;

    use Broadcasts;

    public function getIsActiveAttribute(){
        return $this->status === 'ACTIVE';
    }

    public function getIsDoneAttribute(){
        return $this->status === 'DONE';
    }

    public function broadcastsTo() {
        return new Channel('App.Models.Todo');
    }
}
