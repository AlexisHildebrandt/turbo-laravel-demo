<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response(
            view('todos.index', [
                'todos' => Todo::all()
            ])
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todo = new Todo;

        $todo->title = $request->input('title');

        $todo->save();

        if (request()->wantsTurboStream()) {
            return response()->turboStreamView('todos.turbo_created_stream', [
                'todo' => $todo,
            ]);
        }

        return $this->renderTodoListFrame();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $todo = Todo::find($id);
        if ($request->has('status')) {
            $todo->status = $request->input('status');
        }
        if ($request->has('title')) {
            $todo->title = $request->input('title');
        }

        $todo->save();

        if (request()->wantsTurboStream()) {
            return response()->turboStream($todo);
        }
        return $this->renderTodoListFrame();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Todo::find($id);
        $todo->delete();

        if (request()->wantsTurboStream()) {
            return response()->turboStream($todo);
        }
        return $this->renderTodoListFrame();
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function renderTodoListFrame()
    {
        return response(
            view('todos._todos', [
                'todos' => Todo::all()
            ])
        );
    }
}
