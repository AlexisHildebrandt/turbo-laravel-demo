<form action="/todos" method="post">
    @csrf
    <div class="input-group mb-3">
        <input type="text" name="title" class="form-control" placeholder="New todo" aria-describedby="button-addon2">
        <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Save</button>
    </div>
</form>
