<turbo-stream target="add-todo-form" action="update">
    <template>
        @include('todos._form')
    </template>
</turbo-stream>

<turbo-stream target="todos" action="append">
    <template>
        @include('todos._todo', ['todo' => $todo])
    </template>
</turbo-stream>
