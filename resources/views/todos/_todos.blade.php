<div id="add-todo-form">
    @include('todos._form')
</div>
<ul class="list-group mb-3" id="todos">
    @each('todos._todo', $todos, 'todo')
</ul>
<turbo-echo-stream-source channel="App.Models.Todo" type="channel"/>
