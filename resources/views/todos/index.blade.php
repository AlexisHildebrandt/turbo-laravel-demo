<x-layout>
    <h1 class="mt-5">Todos</h1>
    @include('todos._todos', ['todos' => $todos])
</x-layout>

