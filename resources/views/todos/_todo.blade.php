<li class="list-group-item todo__item--{{strtolower($todo->status)}}" id="@domid($todo)">
    <form action="/todos/{{$todo->id}}" method="post" class="d-inline-block">
        @csrf
        @method('PATCH')
        @if ($todo->isDone)
            <input type="hidden" name="status" value="ACTIVE">
            <button type="submit" class="btn btn-link"><i class="bi bi-check-circle"></i></button>
        @else
            <input type="hidden" name="status" value="DONE">
            <button type="submit" class="btn btn-link"><i class="bi bi-circle"></i></button>
        @endif
    </form>

    {{$todo->title}}

    <form action="/todos/{{$todo->id}}" method="post" class="d-inline-block float-end">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-link"><i class="bi bi-trash"></i></button>
    </form>
</li>
